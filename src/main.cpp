#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "WiFiUdp.h"
#include <string.h>

const char * ssid = "microtrac";
WiFiUDP Udp;
unsigned int localUdpPort = 4210;
char incomingPacket[256];
char replyPacket[] = "Hi there! Got the message :-)";

#define D1 5
#define D2 4
#define D3 0
#define D5 14

uint8_t hydraulics_right_forward = D1;
uint8_t hydraulics_left_forward = D2;
uint8_t hydraulics_right_backward = D3;
uint8_t hydraulics_left_backward = D5;

float drive = 0.0f;
float turn = 0.0f;

void setup() {
  Serial.begin(9600);
  Serial.print("Hello, ESP8266!");

  pinMode(hydraulics_right_forward, OUTPUT);
  pinMode(hydraulics_left_forward, OUTPUT);
  pinMode(hydraulics_right_backward, OUTPUT);
  pinMode(hydraulics_left_backward, OUTPUT);

  digitalWrite(hydraulics_right_forward, LOW);
  digitalWrite(hydraulics_left_forward, LOW);
  digitalWrite(hydraulics_right_backward, LOW);
  digitalWrite(hydraulics_left_backward, LOW);

  WiFi.begin(ssid);
  Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Udp.begin(localUdpPort);
  // Udp.beginMulticast()
  delay(100);
  Serial.printf("Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), localUdpPort);
}

unsigned long lastPacketTime = 0;
unsigned long safetyCriticalThreshold = 100;

void loop() {
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    lastPacketTime = millis();

    // Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
    int len = Udp.read(incomingPacket, 255);
    if (len > 0)
    {
      incomingPacket[len] = '\0';
    }
    // Serial.printf("UDP packet contents: %s\n", incomingPacket);
    char* pch = strtok(incomingPacket, ":");
    char* dim;
    dim = pch;
    pch = strtok(NULL, ":");
    if (strcmp(dim, "drive") == 0)
    {
      drive = atof(pch);
    }
    else if (strcmp(dim, "turn") == 0)
    {
      turn = atof(pch);
    }

    if (drive == 0.0f && turn == 0.0f)
    {
      Serial.println("Stationary");
      digitalWrite(hydraulics_right_forward, LOW);
      digitalWrite(hydraulics_left_forward, LOW);
      digitalWrite(hydraulics_right_backward, LOW);
      digitalWrite(hydraulics_left_backward, LOW);
    }
    else if (abs(drive) > abs(turn))
    {
      if (drive > 0.0)
      {
        Serial.println("Forward");
        digitalWrite(hydraulics_right_forward, HIGH);
        digitalWrite(hydraulics_left_forward, HIGH);
        digitalWrite(hydraulics_right_backward, LOW);
        digitalWrite(hydraulics_left_backward, LOW);
      }
      else
      {
        Serial.println("Backward");
        digitalWrite(hydraulics_right_forward, LOW);
        digitalWrite(hydraulics_left_forward, LOW);
        digitalWrite(hydraulics_right_backward, HIGH);
        digitalWrite(hydraulics_left_backward, HIGH);
      }
    } else
    {
      if (turn > 0) // turn right
      {
        Serial.println("Right");
        digitalWrite(hydraulics_right_forward, LOW);
        digitalWrite(hydraulics_left_forward, HIGH);
        digitalWrite(hydraulics_right_backward, HIGH);
        digitalWrite(hydraulics_left_backward, LOW);
      }
      else // turn left
      {
        Serial.println("Left");
        digitalWrite(hydraulics_right_forward, HIGH);
        digitalWrite(hydraulics_left_forward, LOW);
        digitalWrite(hydraulics_right_backward, LOW);
        digitalWrite(hydraulics_left_backward, HIGH);
      }
    }

    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(replyPacket);
    Udp.endPacket();
  }

  // if we stop receiving UDP packets, disable movement as fail-safe
  if (millis() - lastPacketTime > safetyCriticalThreshold)
  {
    drive = 0.0f;
    turn = 0.0f;
  }
}